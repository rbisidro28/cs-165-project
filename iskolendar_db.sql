.mode column
.headers on
PRAGMA foreign_keys = ON;

--drop table Organization;
--drop table Event;
--drop table Student;

CREATE TABLE Organization 
	(orgid 			int,
	 name			varchar(100) NOT NULL,
	 password		varchar(20) NOT NULL,
	 acronym		varchar(20) NOT NULL,
	 description	text,
	 fb				varchar(100),
	 college		varchar(50),
	 dept 			varchar(50),
	 scope 			varchar(50),
	 PRIMARY KEY (orgid)
	);

CREATE TABLE Event
	(orgname	varchar(100) NOT NULL,
	 name		varchar(200) NOT NULL,
	 etype		varchar(20) NOT NULL,
	 description	text,
	 quota		int,
	 scope		varchar(20) NOT NULL,	
	 year		int NOT NULL,
	 month		int NOT NULL,
	 day 		int NOT NULL,
	 etime		varchar(8) NOT NULL,
	 venue		varchar(50) NOT NULL,
	 PRIMARY KEY (name)
	);	

CREATE TABLE Student
	(sn 		char(10),
	 password	varchar(20) NOT NULL,
	 name		varchar(50) NOT NULL,
	 email		varchar(50),
	 deg_prog	varchar(50) NOT NULL,
	 college	varchar(50) NOT NULL,	
	 PRIMARY KEY (sn)
	);

DELETE FROM Organization;
DELETE FROM Event;
DELETE FROM Student;	

INSERT INTO Organization VALUES
(1, 'UP Alliance of Advocates for the General Welfare of Animals', 'asdasd', 'UP ALAGA', 'UP ALAGA is the premier student organization that aims to educate the public about the rights of animals, and actively champions their protection and improvement of their welfare.', 'fb.com/upalaga', 'N/A', 'N/A', 'university-wide'),
(2, 'UP Association of Civil Engineering Students', 'testing', 'UP ACES', 'As one of the top organizations in the UP College of Engineering, UP ACES continuously promotes academic excellence, professional growth, and social awareness to its members and the community through its relevant events.', 'fb.com/upaces', 'College of Engineering', 'Institute of Civil Engineering', 'program-restricted'),
(3, 'UP Association of Computer Science Majors', 'cursorsexy', 'UP CURSOR', 'Established on August 16, 1983, the organization is geared towards the promotion of computer science and training its members to uphold leadership, teamwork, and social responsibility.', 'fb.com/UPCURSOR', 'College of Engineering', 'Department of Computer Science', 'program-restricted'),
(4, 'UP Cinema Arts Society', '123abc', 'UP CAST', 'UP Cinema Arts Society is a university-wide student organization that aims to train its members in the technical and aesthetic principles of film and video production seeking to contribute with an open eye to Philippine cinema.', 'fb.com/UPCinemaArtsSociety', 'College of Mass Communication', 'N/A', 'university-wide'),
(5, 'UP Graphic Illustration Collaborate', 'hello', 'UP Graphic', 'UP Graphic Illustration Collaborate is a university-wide org based in College of Fine Arts that focuses on the appreciation and practice of illustration and graphic design.', 'fb.com/upgrphc', 'College of Fine Arts', 'N/A', 'university-wide'),
(6, 'UP Junior Philippine Institute of Accountants', 'hahaha', 'UP JPIA', 'UP JPIA was created for the prime purpose of serving as a medium of expression of the ideals and aspirations of current and former Accountancy students of the university.', 'fb.com/UPJPIA', 'College of Business Administration', 'N/A', 'program-restricted');

INSERT INTO Event VALUES
('UP Association of Computer Science Majors', 'Computer Science Summit', 'convention', NULL, 300, 'BS CS majors', 2018, 01, 20, '08:00:00', 'Asian Institute of Management, Makati City'),
('UP Cinema Arts Society', 'Kape''t Pelikula', 'seminar', NULL, 100, 'university-wide', 2018, 04, 14, '09:30:00', 'UP School of Economics Auditorium'),
('UP Junior Philippine Institute of Accountants', 'Accountancy Students'' Seminar and Extra-Curricular Training (ASSET)', 'training seminar', 'ASSET is a five-day nationwide training seminar for the top 50 accountancy students from all over the Philippines.', 50, 'nationwide', 2018, 11, 08, '08:00:00', 'Tagaytay City, Cavite');

INSERT INTO Student VALUES
('201500004', 'xyzxyz', 'Jeon Jungkook', NULL, 'BS Computer Science', 'College of Engineering'),
('201701111', 'shapeofyou', 'Ed Sheeran', 'ed.sheeran@gmail.com', 'BFA Industrial Design', 'College of Fine Arts'),
('201503456', '987654321', 'Frank Ocean', 'focean@up.edu.ph', 'BS Business Administration and Accountancy', 'College of Business Administration'),
('201607070', 'test123', 'Min Yoongi', NULL, 'BS Civil Engineering', 'College of Engineering'),
('201609281', 'yesoryes', 'Daniel Caesar', 'danielcaesar@gmail.com', 'BS Psychology', 'College of Social Sciences and Philosophy'),
('201502483', 'lucky000', 'Jason Mraz', 'jmraz@up.edu.ph', 'BS Computer Science', 'College of Engineering');
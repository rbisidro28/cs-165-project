import os  

from flask import Flask
from flask import render_template, request, redirect, flash, session, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy import and_


project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "iskolendar.db"))

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "493c1b6726fca106125f28c5d55a9726741ba7c2c869ea99"

db = SQLAlchemy(app)

class Organization(db.Model):
    orgid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(20), nullable=False)
    acronym = db.Column(db.String(20), nullable=False)
    description = db.Column(db.Text)
    fb = db.Column(db.String(100))
    college = db.Column(db.String(50), nullable=False)
    dept = db.Column(db.String(50), nullable=False)
    scope = db.Column(db.String(50), nullable=False)

    def create(self):
    	db.session.add(self)
    	db.session.commit()

    def serialize(self):
    	return {
    		"orgid"			: self.orgid,
    		"name"			: self.name,
    		"acronym"		: self.acronym,
    		"description"	: self.description,
    		"fb"			: self.fb,
    		"college"		: self.college,
    		"dept"			: self.dept,
    		"scope"			: self.scope
		}	   	

class Event(db.Model):
	orgname = db.Column(db.String(100), nullable=False)
	name = db.Column(db.String(200), primary_key=True)
	etype = db.Column(db.String(20), nullable=False)
	description = db.Column(db.Text)
	quota = db.Column(db.Integer)
	scope = db.Column(db.String(20), nullable=False)
	year = db.Column(db.Integer, nullable=False)
	month = db.Column(db.Integer, nullable=False)
	day = db.Column(db.Integer, nullable=False)
	etime = db.Column(db.String(8), nullable=False)
	venue = db.Column(db.String(50), nullable=False) 

	def create(self):
		db.session.add(self)
		db.session.commit()

	def serialize(self):
		return {
			"name"			: self.name,
			"etype"			: self.etype,
			"description"	: self.description,
			"quota"			: self.quota,
			"scope"			: self.scope,
			"year"			: self.year,
			"month"			: self.month,
			"day"			: self.day,
			"etime"			: self.etime,
			"venue"			: self.venue
		}		

class Student(db.Model):
	sn = db.Column(db.String(10), primary_key=True)
	password = db.Column(db.String(20), nullable=False)
	name = db.Column(db.String(50), nullable=False)
	email = db.Column(db.String(50))
	deg_prog = db.Column(db.String(50), nullable=False)
	college = db.Column(db.String(50), nullable=False)

	def create(self):
		db.session.add(self)
		db.session.commit()

	def serialize(self):
		return {
			"sn"			: self.sn,
			"name"			: self.name,
			"email"			: self.email,
			"deg_prog"		: self.deg_prog,
			"college"		: self.college,
		}



@app.route('/', methods=["GET", "POST"])
def home():
	events = Event.query.all()
	#print(events)
	un = None
	utype = None
	logged = None
	
	if 'logged_in' in session: 	
		logged = session['logged_in']
	if logged==True:
		un = session['user']['name']
		utype = session['usertype']
	
	return render_template("home.html", events=events, un=un, utype=utype, logged=logged)

@app.route('/login-org', methods=["GET", "POST"])
def login_org():
	if request.method == 'GET':
		return render_template('login-org.html')
	else:
		name = request.form['organization name']
		password = request.form['password']		

		org = Organization.query.filter_by(name=name).first()
		if org is None:
			flash("Organization name does not exist")
		else:
			if org.password != password:
				flash("Organization name and password don't match")
			else:	
				session['logged_in'] = True
				session['usertype'] = 0
				session['user'] = Organization.query.filter_by(name=name).first().serialize()
				return redirect(url_for('home'))

@app.route('/login-student', methods=["GET", "POST"])
def login_student():
	if request.method == 'GET':
		return render_template('login-student.html')
	else:
		sn = request.form['student number']
		password = request.form['password']		

		student = Student.query.filter_by(sn=sn).first()
		if student is None:
			flash("Student number does not exist")
		else:
			if student.password != password:
				flash("Student number and password don't match")
			else:	
				session['logged_in'] = True
				session['usertype'] = 1
				session['user'] = Student.query.filter_by(sn=sn).first().serialize()
				return redirect(url_for('home'))

@app.route('/register-org', methods=["GET", "POST"])
def register_org():
	if request.method == 'POST':
		name = request.form['name']
		password = request.form['password']
		acronym = request.form['acronym']
		description = request.form['description']
		fb = request.form['fb']
		college = request.form['college']
		dept = request.form['dept']
		scope = request.form['scope']

		org = Organization.query.filter_by(name=name).first()
		if org:
			flash("Organization already has an account")
			return render_template('login-org.html')
		else:
			new_org = Organization(name=name, password=password, acronym=acronym, description=description, fb=fb, college=college, dept=dept, scope=scope)
			new_org.create()
			session['logged_in'] = True
			session['usertype'] = 0
			session['user'] = new_org.serialize()
			flash("Registered successfully")
			return redirect(url_for('home'))
	return render_template('register-org.html')

@app.route('/register-student', methods=["GET", "POST"])
def register_student():
	if request.method == 'POST':
		sn = request.form['sn']
		password = request.form['password']
		name = request.form['name']
		email = request.form['email']
		deg_prog = request.form['deg_prog']
		college = request.form['college']

		student = Student.query.filter_by(sn=sn).first()
		if student:
			flash("Student already has an account")
			return render_template('login-student.html')
		else:
			new_student = Student(sn=sn, password=password, name=name, email=email, deg_prog=deg_prog, college=college)
			new_student.create()
			session['logged_in'] = True
			session['usertype'] = 1
			session['user'] = new_student.serialize()
			flash("Registered successfully")
			return redirect(url_for('home'))
	return render_template('register-student.html')


@app.route('/logout')
def logout():
	session['logged_in'] = False
	session.pop('user', None)
	session.pop('usertype', None)
	return redirect(url_for('home'))	

@app.route('/search', methods=["GET", "POST"])
def search():
	if request.method == 'POST':
		etype = request.form['etype']
		orgname = request.form['orgname']

		if etype == "all" and orgname != "":
			results = Event.query.filter_by(orgname=orgname).all()
			#print("A")
		elif etype != "all" and orgname == "":
			results = Event.query.filter_by(etype=etype).all()
			#print("B")
		else:	
			results = Event.query.filter_by(etype=etype, orgname=orgname).all()
			#print("C")
		#print(results)
		return render_template('search-results.html', results=results)
	return render_template('search.html')	

@app.route('/create-event', methods=["GET", "POST"])
def create_event():
	if request.method == 'POST':
		orgname = session['user']['name']
		name = request.form['name']
		etype = request.form['etype']
		description = request.form['description']
		quota = request.form['quota']
		scope = request.form['scope']
		year = request.form['year']
		month = request.form['month']
		day = request.form['day']
		etime = request.form['etime']
		venue = request.form['venue']

		print(orgname)
		event = Event.query.filter_by(name=name).first()
		if event:
			flash("Event already exists")
			return render_template('home.html')
		else:
			new_event = Event(orgname=orgname, name=name, etype=etype, description=description, quota=quota, scope=scope, year=year, month=month, day=day, etime=etime, venue=venue)
			new_event.create()
			flash("Successfully created event")
			return redirect(url_for('home'))
	return render_template('create-event.html')

@app.route('/edit', methods=["GET", "POST"])
def edit():
	if request.method == 'POST':
		toedit = request.form['toedit']
		session['toedit'] = toedit
		return redirect(url_for('edit_event'))
	return render_template('home.html')	

@app.route('/edit-event', methods=["GET","POST"])
def edit_event():
	oldevent = Event.query.get(session['toedit']).serialize()
	if request.method == 'POST':
		newevent = Event.query.get(session['toedit'])
		#print(newevent)

		name = request.form['name']
		etype = request.form['etype']
		description = request.form['description']
		quota = request.form['quota']
		scope = request.form['scope']
		year = request.form['year']
		month = request.form['month']
		day = request.form['day']
		etime = request.form['etime']
		venue = request.form['venue']

		if name != "": newevent.name = name
		if etype != "": newevent.etype = etype
		if description != "": newevent.description = description
		if quota != "": newevent.quota = quota
		if scope != "": newevent.scope = scope
		if year != "": newevent.year = year
		if month != "": newevent.month = month
		if day != "": newevent.day = day
		if etime != "": newevent.etime = etime
		if venue != "": newevent.venue = venue
		db.session.commit() 	
		session.pop('toedit', None)

		return redirect(url_for('home'))
	return render_template('edit-event.html', oldevent=oldevent)

@app.route('/delete', methods=["GET","POST"])
def delete():
	todelete = request.form['todelete']
	event = Event.query.get(todelete)
	db.session.delete(event)
	db.session.commit()
	return redirect(url_for('home'))

@app.route('/profile', methods=["GET","POST"])		    
def profile():
	return render_template('profile.html', info=session['user'])

@app.route('/edit2', methods=["GET", "POST"])		
def edit2():
	if request.method == 'POST':
		toeditprofile = request.form['toeditprofile']
		session['toeditprofile'] = toeditprofile
		return redirect(url_for('edit_profile'))
	return render_template('profile.html')	

@app.route('/edit-profile', methods=["GET", "POST"])
def edit_profile():	
	oldinfo = Student.query.get(session['toeditprofile']).serialize()
	if request.method == 'POST':
		newinfo = Student.query.get(session['toeditprofile'])
		#print(newevent)

		name = request.form['name']
		email = request.form['email']
		deg_prog = request.form['deg_prog']
		college = request.form['college']
		
		if name != "": newinfo.name = name
		if email != "": newinfo.email = email
		if deg_prog != "": newinfo.deg_progog = deg_prog
		if college != "": newinfo.college = college
		db.session.commit() 
		session['user'] = Student.query.get(newinfo.sn).serialize()	
		session.pop('toeditprofile', None)

		return redirect(url_for('profile'))
	return render_template('edit-profile.html', oldinfo=oldinfo)

@app.route('/delete-acc', methods=["GET","POST"])
def delete_acc():
	todelete = request.form['todelete']
	student = Student.query.get(todelete)
	
	print(session)
	session['logged_in'] = False
	session['user'] = None
	session['usertype'] = None

	db.session.delete(student)
	db.session.commit()
	return redirect(url_for('home'))	

if __name__ == "__main__":
    app.run(debug=True)            